<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class VerifyController extends Controller
{
    public function getVerify(){
        return view('verify');
    }

    public function postVerify(Request $request){
        if ($user=User::where('code', $request->code)->first()) {
            $user->active=0;
            $user->code=null;
            $user->save();
            return redirect()->route('home')->withMessage('Your account is active');
            // return message ('Your account is active');

        }
        else{
            return back()->withMessage('Please Verify Your Code'); 
        }
    }
}
