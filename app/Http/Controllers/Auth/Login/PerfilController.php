<?php

namespace App\Http\Controllers\Auth\Login;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\LoginController as DefaultLoginController;

class PerfilController extends DefaultLoginController
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/perfil/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:perfil')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login.perfil');
    }

    public function username()
    {
        return 'username';
    }

    protected function guard()
    {
        return Auth::guard('perfil');
    }



}
