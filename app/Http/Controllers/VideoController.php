<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Video;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        
         if($request->wantsJson()){
         return Video::where('user_id', auth()->id())->get();
         }
    }

        public function store(Request $request)
        {   
            $this->validate($request, [
                'name' => 'required',
                'url' => 'required', 'url'
            ]);
            
            $video = new Video();
            $video->name = $request->name;
            $video->url = $request->url;
            $video->user_id = auth()->id();
            $video->save();
        
            

            return response()->json([$video, 'message' => 'Video Created successfully', 'status', 201]);
        }

    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
         ]);

        $video = Video::find($id);
        $video->name = $request->name;
        $video->url = $request->url;
        $video->save();
        
        return response()->json([$video, 'message' => 'Video Created successfully', 'status', 200]);
    }

    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();
        return response()->json([
         'message' => 'Video deleted successfully', 'status', 200
        ]);
    }
}
