<?php

namespace App\Http\Controllers;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Perfil;
use App\Video;

class PerfilController extends Controller
{

    /**
     * 
     * Trae los perfiles
     *  Retorna los perfiles del usuario que los creó
     */
    public function index(Request $request)
    {
        
         if($request->wantsJson()){
         return Perfil::where('user_id', auth()->id())->get();
         }
    }

    public function store(Request $request)
    {   
        $this->validate($request, [
            'fullname' => 'required',
            'username' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required'],
        ]);

        $perfil = new Perfil();
        $perfil->fullname = $request->fullname;
        $perfil->username = $request->username;
        $perfil->email = $request->email;
        $perfil->password = \Hash::make($request->password);
        $perfil->user_id = auth()->id();
        $perfil->save();
    
        return response()->json($perfil, 201);


    }


    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'fullname' => 'required',
            'username' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required'],
        ]);

        $perfil = Perfil::find($id);
        $perfil->fullname = $request->fullname;
        $perfil->username = $request->username;
        $perfil->email = $request->email;
        $perfil->password = \Hash::make($request->password);
        $perfil->save();
    
        return response()->json([$perfil, 'message' => 'Profile Created successfully', 'status', 200]);
    }

    public function destroy($id)
    {
        $perfil = Perfil::findOrFail($id);
        $perfil->delete();
        return response()->json([
         'message' => 'Profile deleted successfully', 200
        ]);
    }

    public function getVideos(Request $request)
    {
        $videos=DB::table('videos')
        ->join('users', 'videos.user_id' , '=', 'users.id')
        ->join('perfils', 'users.id' , '=', 'perfils.id')
        ->select('users.email', 'perfils.username', 'videos.name', 'videos.url')
        ->get();

        $videos = Video::all();
        return $videos;
    }
}
