<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Model;

class Perfil extends Model
{
    protected $guarded = ['id'];

    protected $hidden = [
    	'perfil_password', 'remember_token',
    ];

    public function getAuthPassword()
    {
    	return $this->perfil_password;
    }
}
