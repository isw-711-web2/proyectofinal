<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Model;

class Perfil extends Model
{
    protected $guarded = ['id'];

    protected $fillable = [
        'fullname', 'username', 'email', 'password', 'code', 'active', 'phone'
    ];

    protected $hidden = [
    	'remember_token',
    ];

    public function getAuthPassword()
    {
    	return $this->password;
    }
}
