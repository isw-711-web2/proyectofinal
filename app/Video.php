<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'name', 'url',
    ];


      //Queryscope
    public function scopeName($query, $name){
        if($name)
        // dd('scope: ' .$nombre);
        return $query->where('name', 'LIKE', "%$name%");
    }
}
