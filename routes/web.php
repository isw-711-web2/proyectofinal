<?php

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'GuestController@welcome');

//Rutas Email Verification
Route::get('/activate-account/{token}', 'GuestController@verify')->name('activate-account');

	

// Route::resource('perfil', 'PerfilController')->middleware('auth');

//Rutas Perfiles
Route::get('perfil', 'PerfilController@index')->middleware('auth');
Route::post('perfil', 'PerfilController@store')->middleware('auth');
Route::put('perfil/{id}', 'PerfilController@update')->middleware('auth');
Route::delete('perfil/{id}', 'PerfilController@destroy')->middleware('auth');
Route::get('video/perfil', 'PerfilController@getVideos');

//Rutas Videos
Route::get('video', 'VideoController@index')->middleware('auth');
Route::post('video', 'VideoController@store')->middleware('auth');
Route::put('video/{id}', 'VideoController@update')->middleware('auth');
Route::delete('video/{id}', 'VideoController@destroy')->middleware('auth');



// Route::get('/home', 'HomeController@index')->name('home');

//Rutas 2Factor Authentication
Route::get('/verify', 'VerifyController@getVerify')->name('getverify');
Route::post('/verify', 'VerifyController@postVerify')->name('verify');


Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');


//Rutas Login Perfil
Route::prefix('perfil')
	->as('perfil.')
    ->group(function() {
        Route::get('home', 'Home\PerfilHomeController@index')->name('home');

    	Route::namespace('Auth\Login')
    		->group(function() {
    			Route::get('login', 'PerfilController@showLoginForm')->name('login');
				Route::post('login', 'PerfilController@login')->name('login');
				Route::post('logout', 'PerfilController@logout')->name('logout');
    		});
	});