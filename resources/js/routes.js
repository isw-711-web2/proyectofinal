
import Perfil from './components/admin/PerfilComponent.vue'
import Playlist from './components/admin/PlaylistComponent.vue'
import PlaylistProfile from './components/admin/PlaylistProfileComponent.vue'
import Video from './components/admin/VideoComponent.vue'
import Filter from './components/admin/FilterComponent.vue'
export const routes = [
    {
        path:'/VideosProfiles',
        component:PlaylistProfile
    },
    { 
        path:'/perfils',
        component:Perfil
    },
    { 
        path:'/playlists',
        component:Playlist
    },
    { 
        path:'/filter',
        component:Filter
    },
    { 
        path:'/video',
        component:Video
    },
];