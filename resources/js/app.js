require('./bootstrap');

window.Vue = require('vue');

// Vue.use(VuePlayer)

import Embed from 'v-video-embed'
Vue.use(Embed);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('filter-component', require('./components/admin/FilterComponent.vue').default);
Vue.component('video-profile', require('./components/admin/PlaylistProfileComponent.vue').default);
//Import progressbar
require('./progressbar'); 

//Setup custom events 
require('./customEvents'); 

//Import View Router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

//Import Sweetalert2
import Swal from 'sweetalert2'
window.Swal = Swal
const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})
window.Toast = Toast

//Import v-from
import { Form, HasError, AlertError } from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

//Routes
import { routes } from './routes';

//Register Routes
const router = new VueRouter({
    routes, 
    mode: 'hash',

})

const app = new Vue({
    el: '#app',
    router
});