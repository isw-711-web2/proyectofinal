@extends('layouts.app')

@section('navigation')
    @include('navigation.perfil')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center">Dashboard</div>
                <div class="card-body">
                    <video-profile></videos-profile>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection