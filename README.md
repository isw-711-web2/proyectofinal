TubeKids

Started

## Installation
To install the PHP client library using Composer:
composer require nexmo/laravel

Alternatively, add these two lines to your composer require section:
{
    "require": {
        "nexmo/laravel": "^2.0"
    }
}
Add Nexmo\Laravel\NexmoServiceProvider to the providers array in your config/app.php:
'providers' => [
    // Other service providers...

    Nexmo\Laravel\NexmoServiceProvider::class,
],
Or add an alias in your config/app.php:

'aliases' => [
    ...
    'Nexmo' => Nexmo\Laravel\Facade\Nexmo::class,
],
You can use artisan vendor:publish to copy the distribution configuration file to your app's config directory:
php artisan vendor:publish

Then update config/nexmo.php with your credentials. Alternatively, you can update your .env file with the following:
NEXMO_KEY=my_api_key
NEXMO_SECRET=my_secret

## Vue js
composer require laravel/ui
php artisan ui vue

